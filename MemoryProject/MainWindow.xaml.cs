﻿using MemoryProject.UI;
using MemoryProject.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MemoryProject {
    /// <summary>
    /// De window waarop de verschillende paginas van het spel worden weergeven.
    /// </summary>
    public partial class MainWindow : Window {
        private MemoryGamePage ActivePage;
        private BackgroundManager BackgroundManager;

        public MainWindow() {
            this.ActivePage = null;

            InitializeComponent();
            this.WindowState = WindowState.Maximized;

            // Initialiseer de SettingsManager met enkele standaardobjecten.
            Settings.DefaultGlobals.RegisterAll();
            Settings.SettingsManager.Instance["Window"] = new Settings.Setting(this, "De huidige Window.", null, false);

            this.BackgroundManager = new BackgroundManager(this);
            Settings.SettingsManager.Instance["BGManager"] = new Settings.Setting(BackgroundManager, "Manager van de achtergrondafbeelding.", null, false);

            // Zorg dat geluiden standaard vooraf geladen worden.
            void LoadSounds(string theme) {
                SoundManager.ClearCache();

                string[] files = Utility.FileIO.ListFiles("./" + theme + "/sfx/");
                foreach (string file in files) {
                    if (System.IO.Path.GetExtension(file) == ".wav") SoundManager.PreLoadSound(System.IO.Path.GetFileNameWithoutExtension(file), theme);
                }
            }

            Settings.SettingsManager.Instance["Thema"].AddListener(delegate (object OldValue, object NewValue) {
                LoadSounds((string) NewValue);
            });

            LoadSounds((string) Settings.SettingsManager.Instance["Thema"].Value);

            // Stuur toetsenbordinput door naar de KeyboardManager
            KeyEventHandler handler = delegate (object source, KeyEventArgs args) {
                KeyboardManager.Instance.DispatchKeypress(source, args);
            };

            this.PreviewKeyDown += handler;
            this.PreviewKeyUp += handler;

            // Begin het spel op het hoofdmenu.
            SetActivePage(new UI.LoadingPage());
        }

        protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo) {
            base.OnRenderSizeChanged(sizeInfo);

            double h = this.ActualHeight;
            double w = this.ActualWidth;
        }

        /// <summary>
        /// Toon een nieuwe pagina op het scherm.
        /// </summary>
        /// <param name="page">De pagina die getoond moet worden.</param>
        public void SetActivePage(MemoryGamePage page) {
            if (this.ActivePage != null) this.ActivePage.OnPageHidden();

            this.ActivePage = page;
            this.Content = page;

            this.ActivePage.OnPageShown();
        }

        /// <summary>
        /// Verkrijg de pagina die op dit moment getoond wordt.
        /// </summary>
        /// <returns>De pagina die op dit moment getoond wordt.</returns>
        public MemoryGamePage GetActivePage() {
            return this.ActivePage;
        }
    }
}
