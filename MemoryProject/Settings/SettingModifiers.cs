﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace MemoryProject.Settings {
    /// <summary>
    /// Het object dat gemaakt wordt door een SettingChanger.
    /// Bevat een UIElement en een functie waarmee de waarde in dat UIElement verkregen kan worden.
    /// </summary>
    public struct SettingUIElement {
        public delegate string GetSettingValue();

        public UIElement Element;
        public GetSettingValue GetValue;

        public SettingUIElement(UIElement element, GetSettingValue getValue) {
            this.Element = element;
            this.GetValue = getValue;
        }
    }

    /// <summary>
    /// De SettingsModifiers zijn een collectie van functies die UIElements genereren waarmee de gebruiker de bijbehorende Setting kan aapassen.
    /// Een Setting kan aan een functie (een SettingChanger) gelinkt worden door de Setting het modifierID van die SettingChanger mee te geven. (De Key in de collectie.)
    /// </summary>
    public static class SettingModifiers {
        public delegate SettingUIElement SettingChanger(Setting setting);

        public static readonly Dictionary<string, SettingChanger> Modifiers = new Dictionary<string, SettingChanger> {
            { "MOD_TEXTBOX",      new SettingChanger(ModTextbox)       },
            { "MOD_THEME_SELECT", new SettingChanger(ModThemeSelector) },
            { "MOD_VOL_SLIDER",   new SettingChanger(ModVolumeSlider)  }
        };


        /// <summary>
        /// Een simpele tekstbox om een String-Setting mee te veranderen.
        /// </summary>
        /// <param name="setting">De bijbehorende Setting.</param>
        /// <returns>Het aangemaakte element.</returns>
        private static SettingUIElement ModTextbox(Setting setting) {
            TextBox box = new TextBox();
            box.Text = setting.Value.ToString();

            return new SettingUIElement(box, delegate () { return box.Text; });
        }


        /// <summary>
        /// Een lijst met alle beschikbare themas die gebruikt kunnen worden voor het spel.
        /// Themas worden alleen in de lijst gezet als ze de juist bestanden bevatten.
        /// </summary>
        /// <param name="setting">De bijbehorende Setting.</param>
        /// <returns>Het aangemaakte element.</returns>
        private static List<string> ThemeSelectorRequiredFiles = new List<string> {
            "front.png", "000.png", "001.png", "bg.gif"
        };

        private static SettingUIElement ModThemeSelector(Setting setting) {
            List<string> themes = Utility.FileIO.ListFolders("./").ToList();
            ComboBox box = new ComboBox();

            foreach (string theme in themes) {
                bool valid = true;
                foreach (string file in ThemeSelectorRequiredFiles) {
                    if (!Utility.FileIO.Exists(theme + "/" + file)) valid = false;
                }

                if (valid) {
                    ComboBoxItem item = new ComboBoxItem();
                    item.Content = theme.Substring(2);
                    box.Items.Add(item);

                    // Substring vanaf 2, want de themamapnaam bevat './' aan het begin.
                    if (theme.Substring(2) == setting.Value.ToString()) item.IsSelected = true;
                }
            }

            return new SettingUIElement(box, delegate () { return ((ComboBoxItem) box.SelectedItem).Content.ToString(); });
        }


        /// <summary>
        /// Een slider om een volume mee aan te passen.
        /// </summary>
        /// <param name="setting">De setting die wordt aangepast.</param>
        /// <returns>Het aangemaakte element</returns>
        private static SettingUIElement ModVolumeSlider(Setting setting) {
            Grid container = new Grid();

            int[] widths = { 8, 1 };
            for (int i = 0; i < widths.Count(); i++) {
                ColumnDefinition def = new ColumnDefinition();
                def.Width = new GridLength(widths[i], GridUnitType.Star);
                container.ColumnDefinitions.Add(def);
            }

            Slider slider = new Slider();
            slider.Maximum = 100;
            slider.Value = Double.Parse((string) setting.Value) * 100;

            slider.TickPlacement = System.Windows.Controls.Primitives.TickPlacement.BottomRight;
            slider.TickFrequency = 5.0;

            Label value = new Label();
            value.Content = (int) slider.Value + "%";
            value.HorizontalAlignment = HorizontalAlignment.Center;

            slider.ValueChanged += delegate (object source, RoutedPropertyChangedEventArgs<double> args) {
                value.Content = (int) slider.Value + "%";
            };

            Grid.SetColumn(slider, 0);
            Grid.SetColumn(value, 1);
            container.Children.Add(slider);
            container.Children.Add(value);

            return new SettingUIElement(
                container,
                delegate () { return (slider.Value / 100).ToString(); }
            );
        }
    }
}
