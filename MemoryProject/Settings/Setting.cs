﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MemoryProject.Settings {
    /// <summary>
    /// Een Setting is de combinatie van een waarde, (Value) en een beschrijving van die waarde. (Description)
    /// Een Setting kan voor de gebruiker zijn, (IsUserSetting = true) of een interne waarde die door het spel zelf wordt gehandhaafd. (IsUserSetting = false)
    /// Aan iedere Setting kunnen een of meer listeners toegevoegd worden die getriggerd worden als de waarde van de Setting verandert.
    /// Aangezien de door de gebruiker instelbare Settings worden geserialiseerd, moeten deze van en naar het type String converteerbaar zijn.
    /// Interne Settings hebben deze beperking niet.
    /// Als de Setting een door de gebruiker aanpasbare setting is, is modifierID het ID van de SettingChanger die het UIElement genereert om die Setting mee aan te passen.
    /// </summary>
    public class Setting {
        public delegate void OnSettingChanged(object OldValue, object NewValue);

        public readonly string Description;
        public readonly string ModifierID;
        public readonly bool IsUserSetting;
        private object value;
        private List<OnSettingChanged> listeners;

        public Setting(object value, string description, string modifierID, bool isUserSetting = true) {
            this.Description = description;
            this.ModifierID = modifierID;
            this.IsUserSetting = isUserSetting;
            this.value = value;
            this.listeners = new List<OnSettingChanged>();
        }

        public object Value {
            get { return this.value; }
            set {
                foreach (OnSettingChanged listener in listeners) listener(this.value, value);
                this.value = value;
            }
        }

        public void AddListener(OnSettingChanged listener) { listeners.Add(listener); }
        public void RemoveListener(OnSettingChanged listener) { listeners.Remove(listener); }
    }
}
