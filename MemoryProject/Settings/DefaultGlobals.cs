﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace MemoryProject.Settings {
    /// <summary>
    /// Bevat enkele objecten die aan de start van het programma aan de SettingsManager worden toegevoegd.
    /// </summary>
    public static class DefaultGlobals {
        public static void RegisterAll() {
            SettingsManager mgr = SettingsManager.Instance;

            // Alle standaardbrushes zijn ondoorzichtig, dus we maken een nieuwe.
            SolidColorBrush HalfGrayBrush = new SolidColorBrush(Colors.LightGray);
            HalfGrayBrush.Opacity = 0.55;

            mgr["HalfGrayBrush"] = new Setting(HalfGrayBrush, "Een gedeeltelijk doorzichtbare grijze brush.", null, false);
        }
    }
}
