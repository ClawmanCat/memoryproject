﻿using MemoryProject.Serialization;
using MemoryProject.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MemoryProject.Settings {
    /// <summary>
    /// De SettingsManager bevat de instellingen van het spel en gecashde objecten zoals de achtergrondafbeelding.
    /// </summary>
    public class SettingsManager {
        public static SettingsManager Instance = new SettingsManager();

        private Dictionary<string, Setting> settings;


        private SettingsManager() {
            this.settings = new Dictionary<string, Setting>();

            // Standaardwaarden. Deze kunnen worden overschreven door het laden van de SettingsManager vanuit het bestand.
            this["Thema"]        = new Setting("StarWars",       "Het thema van het spel.",            "MOD_THEME_SELECT");
            this["Speler 1"]     = new Setting("Katternije",     "De naam van Speler 1.",              "MOD_TEXTBOX");
            this["Speler 2"]     = new Setting("Spiedeldomdije", "De naam van Speler 2.",              "MOD_TEXTBOX");
            this["Volume"]       = new Setting("1.0",            "Het volume van het spel.",           "MOD_VOL_SLIDER");
            this["Effectvolume"] = new Setting("1.0",            "Het volume van de geluidseffecten.", "MOD_VOL_SLIDER");
            this["Muziekvolume"] = new Setting("1.0",            "Het volume van de muziek.",          "MOD_VOL_SLIDER");

            FromFile();
        }


        ~SettingsManager() {
            ToFile();
        }


        /// <summary>
        /// Maakt het mogelijk om de SettingsManager als een array te indexeren met [].
        /// Het is niet toegestaan om een Setting toe te voegen met een Key die reeds in de SettingsManager bestaat.
        /// </summary>
        /// <param name="Key">De naam van de Setting die verkregen of toegevoegd wordt.</param>
        /// <returns></returns>
        public Setting this[string Key] {
            get { return settings[Key]; }
            set {
                // Settings kunnen niet worden overschreven. Verander in dit geval de Value van de Setting, of creeer een nieuwe.
                if (settings.ContainsKey(Key)) throw new ArgumentException("De SettingsManager bevat reeds een Setting met de naam " + Key);
                settings[Key] = value;
            }
        }


        /// <summary>
        /// Verkrijg een readonly-versie van de Settings om makkelijk te kunnen itereren.
        /// </summary>
        /// <returns>Een readonly-versie van de Settings.</returns>
        public IReadOnlyDictionary<string, Setting> Data() {
            return this.settings;
        }


        /// <summary>
        /// Laadt de SettingsManager vanuit het bestand ./settings.dat.
        /// Waarden die reeds in de SettingsManager staan kunnen worden overschreven.
        /// </summary>
        public void FromFile() {
            if (!FileIO.Exists("./settings.dat")) return;

            foreach (SerializedSettings.SerializedSetting st in new SerializedSettings(FileIO.ReadData("./settings.dat")).settings) {
                if (!settings.ContainsKey(st.id)) this[st.id] = new Setting(st.value, st.description, st.setter);
                else this[st.id].Value = st.value;
            }
        }


        /// <summary>
        /// Laadt de SettingsManager naar het bestand ./settings.dat.
        /// </summary>
        public void ToFile() {
            List<SerializedSettings.SerializedSetting> serialized = new List<SerializedSettings.SerializedSetting>();
            foreach (var pair in settings) {
                if (!pair.Value.IsUserSetting) continue;

                SerializedSettings.SerializedSetting st = new SerializedSettings.SerializedSetting(
                    pair.Key,
                    pair.Value.ModifierID,
                    (string) pair.Value.Value,
                    pair.Value.Description
                );

                serialized.Add(st);
            }

            FileIO.WriteData("./settings.dat", new SerializedSettings(serialized).array);
        }
    }
}
