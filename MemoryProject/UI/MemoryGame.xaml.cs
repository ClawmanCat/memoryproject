﻿using MemoryProject.Serialization;
using MemoryProject.UI.Animation;
using MemoryProject.UI.Game;
using MemoryProject.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MemoryProject.UI {
    /// <summary>
    /// De pagina die de MemoryGame bevat.
    /// </summary>
    public partial class MemoryGame : MemoryGamePage {
        private MemoryGrid grid;
        private Player player1, player2;
        private Timer updateTimer;
        private PauseMenu pauseMenu;
        private bool isPaused;
        private bool gameStarted;

        public MemoryGame(SerializedGame savefile = null) {
            InitializeComponent();

            this.pauseMenu = new PauseMenu(this);
            Grid.SetRow(pauseMenu, 1);
            Grid.SetColumn(pauseMenu, 1);

            this.gameStarted = false;

            SolidColorBrush bg = (SolidColorBrush) Settings.SettingsManager.Instance["HalfGrayBrush"].Value;
            TopbarBG.Background = bg;
            BtmbarBG.Background = bg;

            if (savefile != null) {
                Settings.SettingsManager.Instance["Speler 1"].Value = savefile.p1Name;
                Settings.SettingsManager.Instance["Speler 2"].Value = savefile.p2Name;
            }

            this.player1 = new Player((string) Settings.SettingsManager.Instance["Speler 1"].Value);
            this.player2 = new Player((string) Settings.SettingsManager.Instance["Speler 2"].Value);

            if (savefile != null) {
                player1.score = savefile.p1Score;
                player2.score = savefile.p2Score;
            }

            // Laad het grid.
            this.grid = (savefile == null) ? new MemoryGrid(player1, player2) : new MemoryGrid(savefile, player1, player2);
            Grid.SetRow(grid, 1);
            Grid.SetColumn(grid, 1);
            MemoryGameGrid.Children.Add(grid);

            // Zorg dat de topbar iedere 100ms wordt geupdatet.
            this.updateTimer = new Timer();
            this.updateTimer.Interval = 100;
            this.updateTimer.AutoReset = true;
            this.updateTimer.Elapsed += delegate (object source, ElapsedEventArgs args) { this.Dispatcher.Invoke(delegate () { UpdateTopbar(); }); };
            this.updateTimer.Start();

            UpdateTopbar();

            // Start de countdown.
            Border countdownWrapper = new Border();
            countdownWrapper.CornerRadius = new CornerRadius(200);
            countdownWrapper.BorderBrush = Brushes.Black;
            countdownWrapper.BorderThickness = new Thickness(5);
            countdownWrapper.Width = 500;
            countdownWrapper.Height = 500;
            countdownWrapper.Background = (SolidColorBrush) Settings.SettingsManager.Instance["HalfGrayBrush"].Value;

            Label countdown = new Label();
            countdown.Margin = new Thickness(-100);
            countdownWrapper.Child = countdown;

            Grid.SetRow(countdownWrapper, 1);
            Grid.SetColumn(countdownWrapper, 1);
            MemoryGameGrid.Children.Add(countdownWrapper);

            Countdown countdownTimer = new Countdown(3, countdown, delegate () {
                MemoryGameGrid.Children.Remove(countdownWrapper);
                grid.Start();
                gameStarted = true;
            });

            KeyboardManager.Instance.AddListener(delegate (Key key, bool isDown) {
                if (key == Key.Escape && isDown) TogglePause();
            });
        }


        public SerializedGame Serialize() {
            return this.grid.Serialize();
        }


        private void UpdateTopbar() {
            P1Name.Content = this.player1.name;
            P2Name.Content = this.player2.name;

            P1Score.Content = "Score: " + this.player1.score;
            P2Score.Content = "Score: " + this.player2.score;

            TimeValue.Content = TimeSpan.FromMilliseconds(grid.timer.GetGameTimeRemaining()).ToString("hh\\:mm\\:ss");
            TurnValue.Content = TimeSpan.FromMilliseconds(grid.timer.GetTurnTimeRemaining()).ToString("hh\\:mm\\:ss");

            SolidColorBrush ActivePlayerBG = new SolidColorBrush(Colors.Green);
            ActivePlayerBG.Opacity = 0.55;

            SolidColorBrush OtherPlayerBG = new SolidColorBrush(Colors.DarkGray);
            OtherPlayerBG.Opacity = 0.55;

            if (grid.getActivePlayer() == this.player1) {
                P1.Background = ActivePlayerBG;
                P2.Background = OtherPlayerBG;
            } else {
                P1.Background = OtherPlayerBG;
                P2.Background = ActivePlayerBG;
            }
        }

        public override void OnPageHidden() {
            updateTimer.Stop();
            grid.OnGameDone(false);

            base.OnPageHidden();
        }

        public void TogglePause() {
            if (!gameStarted) return;

            if (isPaused) {
                MemoryGameGrid.Children.Remove(pauseMenu);
                grid.timer.Resume();
            } else {
                MemoryGameGrid.Children.Add(pauseMenu);
                grid.timer.Pause();
            }

            isPaused = !isPaused;
        }
    }
}
