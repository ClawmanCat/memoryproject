﻿using MemoryProject.Serialization;
using MemoryProject.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MemoryProject.UI {
    /// <summary>
    /// Het hoofdmenu.
    /// </summary>
    public partial class MainMenu : MemoryGamePage {
        private List<Label> buttons;

        public MainMenu() : base() {
            InitializeComponent();
            if (!Utility.SoundManager.IsPlaying("theme")) Utility.SoundManager.PlaySound("theme", true);

            this.buttons = new List<Label> { LocalGameLabel, ResumeGameLabel, HighScoresLabel, SettingsLabel };

            foreach (Label btn in buttons) {
                btn.Background = (SolidColorBrush) Settings.SettingsManager.Instance["HalfGrayBrush"].Value;
            }
        }


        protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo) {
            MainMenuGrid.Height = sizeInfo.NewSize.Height;
            base.OnRenderSizeChanged(sizeInfo);
        }


        private void OnGameButtonClick(object sender, MouseButtonEventArgs args) {
            Utility.SoundManager.PlaySound("menu");
            ((MainWindow) Settings.SettingsManager.Instance["Window"].Value).SetActivePage(new MemoryGame());
        }


        private void OnResumeButtonClick(object sender, MouseButtonEventArgs args) {
            Utility.SoundManager.PlaySound("menu");
            ((MainWindow) Settings.SettingsManager.Instance["Window"].Value).SetActivePage(new SaveMenuWrapper(this, delegate (SerializedGame game) {
                ((MainWindow) Settings.SettingsManager.Instance["Window"].Value).SetActivePage(new MemoryGame(game));
            }));
        }


        private void OnHighScoresButtonClick(object sender, MouseButtonEventArgs args) {
            Utility.SoundManager.PlaySound("menu");
            ((MainWindow) Settings.SettingsManager.Instance["Window"].Value).SetActivePage(new HighScoresPage());
        }


        private void OnSettingsButtonClick(object sender, MouseButtonEventArgs args) {
            Utility.SoundManager.PlaySound("menu");
            ((MainWindow) Settings.SettingsManager.Instance["Window"].Value).SetActivePage(new SettingsMenu());
        }
    }
}
