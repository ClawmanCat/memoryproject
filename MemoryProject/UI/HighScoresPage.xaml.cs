﻿using MemoryProject.Settings;
using MemoryProject.UI.Game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MemoryProject.UI {
    /// <summary>
    /// Een lijst van High Scores.
    /// </summary>
    public partial class HighScoresPage : MemoryGamePage {
        private Label active;

        public HighScoresPage() {
            InitializeComponent();

            ContentGrid.Background = (SolidColorBrush) SettingsManager.Instance["HalfGrayBrush"].Value;

            List<Label> sortLabels = GetSortLabels();
            int count = 0;
            foreach (Label label in sortLabels) {
                GridTypeSelector.ColumnDefinitions.Add(new ColumnDefinition());
                Grid.SetColumn(label, count++);
                GridTypeSelector.Children.Add(label);
            }

            UpdateTopbar(sortLabels[0]);
            SetShownScores(GetVisibleScores(sortLabels[0]));
        }

        private List<HighScores.HighScore> GetVisibleScores(Label sort) {
            int num = (int) sort.Tag;

            List<HighScores.HighScore> scores = new List<HighScores.HighScore>();
            foreach (HighScores.HighScore score in HighScores.Instance.Data()) {
                if (score.numCards == num || num == -1) scores.Add(score);
            }

            return scores;
        }

        private void SetShownScores(List<HighScores.HighScore> scores) {
            HighScoresList.Children.Clear();
            HighScoresList.RowDefinitions.Clear();

            scores = scores.OrderByDescending(s => s.score).ToList();

            int count = 0;
            foreach (HighScores.HighScore score in scores) {
                Grid scoreLabel = new Grid();
                scoreLabel.Background = Brushes.DarkGray;
                scoreLabel.MaxHeight = 25;
                scoreLabel.Margin = new Thickness(10, 0, 10, 10);

                scoreLabel.ColumnDefinitions.Add(new ColumnDefinition());  // Naam
                scoreLabel.ColumnDefinitions.Add(new ColumnDefinition());  // Configuratie
                scoreLabel.ColumnDefinitions.Add(new ColumnDefinition());  // Score

                Label name = new Label();
                name.Content = score.name;
                Grid.SetColumn(name, 0);
                scoreLabel.Children.Add(name);

                Label config = new Label();
                config.Content = score.numCards + " Kaarten";
                Grid.SetColumn(config, 1);
                scoreLabel.Children.Add(config);

                Label value = new Label();
                value.Content = score.score + " Punten";
                Grid.SetColumn(value, 2);
                scoreLabel.Children.Add(value);

                RowDefinition def = new RowDefinition();
                def.MaxHeight = scoreLabel.MaxHeight + 10;

                HighScoresList.RowDefinitions.Add(def);
                Grid.SetRow(scoreLabel, count++);
                HighScoresList.Children.Add(scoreLabel);
            }
        }

        private List<Label> GetSortLabels() {
            List<Label> labels = new List<Label>();
            List<int> sorts = new List<int>();

            void OnClick(Label label) {
                UpdateTopbar(label);
                SetShownScores(GetVisibleScores(label));
            }

            Label all = new Label();
            all.Content = "Alle Configuraties";
            all.Margin = new Thickness(10);
            all.Background = Brushes.DarkGray;
            all.HorizontalAlignment = HorizontalAlignment.Center;
            all.VerticalAlignment = VerticalAlignment.Center;
            all.Tag = -1;
            all.PreviewMouseLeftButtonDown += delegate (object source, MouseButtonEventArgs args) { OnClick(all); };

            labels.Add(all);

            foreach (HighScores.HighScore score in HighScores.Instance.Data()) {
                if (sorts.Contains(score.numCards)) continue;

                sorts.Add(score.numCards);

                Label label = new Label();
                label.Content = score.numCards + " Kaarten";
                label.Margin = new Thickness(10);
                label.Background = Brushes.DarkGray;
                label.HorizontalAlignment = HorizontalAlignment.Center;
                label.VerticalAlignment = VerticalAlignment.Center;
                label.Tag = score.numCards;
                label.PreviewMouseLeftButtonDown += delegate (object source, MouseButtonEventArgs args) { OnClick(label); };

                labels.Add(label);
            }

            labels = labels.OrderBy(l => (int) l.Tag).ToList();
            return labels;
        }

        private void UpdateTopbar(Label clicked) {
            if (active != null) active.Background = Brushes.DarkGray;

            clicked.Background = Brushes.Green;
            active = clicked;
        }

        private void OnReturnButtonClicked(object sender, MouseButtonEventArgs e) {
            ((MainWindow) SettingsManager.Instance["Window"].Value).SetActivePage(new MainMenu());
        }
    }
}
