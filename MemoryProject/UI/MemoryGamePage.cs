﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace MemoryProject.UI {
    /// <summary>
    /// Basis voor alle Pages in dit project.
    /// </summary>
    public class MemoryGamePage : Page {
        public MemoryGamePage() : base() {}

        /// <summary>
        /// Wordt uitgevoerd als de pagina in de MainWindow wordt weergeven.
        /// </summary>
        public virtual void OnPageShown() { }

        /// <summary>
        /// Wordt uitgevoerd als de pagina niet meer in de MainWindow wordt weergeven.
        /// </summary>
        public virtual void OnPageHidden() { }
    }
}
