﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace MemoryProject.UI.Animation {
    /// <summary>
    /// Timer die zorgt dat de kaarten nog even zichtbaar blijven na het omdraaien.
    /// </summary>
    public class CardShowTimer {
        public delegate void OnCardShowDone();

        public const double CARD_SHOW_DURATION = 1000;

        public CardShowTimer(OnCardShowDone onDone) {
            Timer timer = new Timer();
            timer.Interval = CARD_SHOW_DURATION;
            timer.AutoReset = false;

            timer.Elapsed += delegate (object source, ElapsedEventArgs args) {
                onDone();
            };

            timer.Start();
        }

    }
}
