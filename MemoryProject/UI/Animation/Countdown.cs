﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace MemoryProject.UI.Animation {
    /// <summary>
    /// Weergeeft een aftelling op het gegeven label.
    /// </summary>
    public class Countdown {
        public delegate void OnCountdownDone();

        public Countdown(int from, Label target, OnCountdownDone onDone) {
            target.HorizontalAlignment = HorizontalAlignment.Center;
            target.VerticalAlignment = VerticalAlignment.Center;
            target.HorizontalContentAlignment = HorizontalAlignment.Center;
            target.VerticalContentAlignment = VerticalAlignment.Center;

            Label wrappedLabel = new Label();
            wrappedLabel.FontSize = 500;
            wrappedLabel.Foreground = Brushes.Red;
            wrappedLabel.Margin = new Thickness(-500, -500, -500, -420);
            wrappedLabel.HorizontalAlignment = HorizontalAlignment.Center;
            wrappedLabel.VerticalAlignment = VerticalAlignment.Center;

            target.Width = 500;
            target.Height = 500;

            Timer timer = new Timer();
            timer.Interval = 1000;
            timer.AutoReset = true;

            int at = from;
            timer.Elapsed += delegate (object source, ElapsedEventArgs args) {
                target.Dispatcher.Invoke(delegate () {
                    wrappedLabel.Content = at.ToString();
                    target.Content = wrappedLabel;

                    if (at < 0) {
                        timer.Stop();
                        onDone();
                    }

                    at--;
                });
            };

            timer.Start();
        }
    }
}
