﻿using MemoryProject.UI.Game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace MemoryProject.UI.Animation {
    /// <summary>
    /// Class om de animaties van het draaien en verdwijnen van kaarten te regelen.
    /// Regelt tevens dat de kaart echt omdraait/verdwijnt door flip() of erase() met animate = false aan te roepen.
    /// </summary>
    public static class CardAnimation {
        public delegate void OnAnimationDone();

        // De tijd om een kaart om te draaien en hoe vaak de kaart tijdens het draaien geupdatet wordt.
        public const double FLIP_DURATION  = 125.0;
        public const double FLIP_NO_FRAMES = 15.0;

        // De tijd om een kaart om te verdwijnen en hoe vaak de kaart tijdens het verdwijnen geupdatet wordt.
        public const double ERASE_DURATION  = 125.0;
        public const double ERASE_NO_FRAMES = 15.0;


        public static void FlipCard(MemoryCard card, OnAnimationDone onDone) {
            card.setAnimated(true);

            Timer timer = new Timer();
            timer.Interval = (FLIP_DURATION / FLIP_NO_FRAMES);
            timer.AutoReset = true;

            int count = 0;
            int half_frames = (int) (FLIP_NO_FRAMES / 2);
            timer.Elapsed += delegate (object source, ElapsedEventArgs args) {
                card.Dispatcher.Invoke(delegate () {
                    if (count < half_frames) {
                        // 1e helft: kaart wordt kleiner.
                        FlipTransform(card.getShownImageSource(), card.getShownImage(), 1.0 - (count / (FLIP_NO_FRAMES / 2)));
                    } else if (count == half_frames) {
                        // 1e helft voorbij: kaart draait.
                        card.flip(null, false);
                        FlipTransform(card.getShownImageSource(), card.getShownImage(), 0);
                    } else if (count < FLIP_NO_FRAMES) {
                        // 2e helft: kaart wordt breder.
                        FlipTransform(card.getShownImageSource(), card.getShownImage(), ((count - half_frames) / (FLIP_NO_FRAMES / 2)));
                    } else {
                        // 2e helft voorbij: animatie eindigt.
                        card.getShownImage().Source = card.getShownImageSource().Source;
                        card.getHiddenImage().Source = card.getHiddenImageSource().Source;
                        card.setAnimated(false);

                        timer.Stop();

                        if (onDone != null) onDone();
                    }

                    count++;
                });
            };

            timer.Start();
        }


        private static void FlipTransform(Image source, Image dest, double width) {
            dest.Source = new TransformedBitmap((BitmapSource) source.Source, new ScaleTransform(width, 1, 0, 0));
        }


        public static void EraseCard(MemoryCard card, OnAnimationDone onDone) {
            card.setAnimated(true);

            Timer timer = new Timer();
            timer.Interval = (ERASE_DURATION / ERASE_NO_FRAMES);
            timer.AutoReset = true;
            

            int count = 0;
            timer.Elapsed += delegate (object source, ElapsedEventArgs args) {
                card.Dispatcher.Invoke(delegate () {
                    if (count < ERASE_NO_FRAMES) {
                        card.Dispatcher.Invoke(delegate () { card.Opacity = 1.0 - (count / ERASE_NO_FRAMES); });
                    } else {
                        card.remove(null, false);
                        card.setAnimated(false);

                        timer.Stop();

                        if (onDone != null) onDone();
                    }

                    count++;
                });
            };

            timer.Start();
        }
    }
}
