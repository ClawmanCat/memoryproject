﻿using MemoryProject.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace MemoryProject.UI {
    /// <summary>
    /// Het SaveMenu staat de gebruiker toe om een opgeslagen spel te selecteren.
    /// </summary>
    public class SaveMenu : Grid {
        public delegate void OnSaveChosen(SerializedGame game);

        public SaveMenu(MemoryGamePage source, OnSaveChosen onChosen) {
            SolidColorBrush bg = (SolidColorBrush)Settings.SettingsManager.Instance["HalfGrayBrush"].Value;

            this.Background = bg;
            this.Margin = new Thickness(100, 100, 100, 100);

            ColumnDefinition centerCol = new ColumnDefinition();
            centerCol.Width = new GridLength(4, GridUnitType.Star);

            RowDefinition centerRow = new RowDefinition();
            centerRow.Height = new GridLength(4, GridUnitType.Star);

            this.ColumnDefinitions.Add(new ColumnDefinition());
            this.ColumnDefinitions.Add(centerCol);
            this.ColumnDefinitions.Add(new ColumnDefinition());

            this.RowDefinitions.Add(new RowDefinition());
            this.RowDefinitions.Add(centerRow);
            this.RowDefinitions.Add(new RowDefinition());

            TextBlock title = new TextBlock();
            title.Text = "Opgeslagen Spellen";
            title.Foreground = Brushes.DarkCyan;
            title.FontSize = 64;
            title.HorizontalAlignment = HorizontalAlignment.Center;
            title.VerticalAlignment = VerticalAlignment.Center;
            Grid.SetColumnSpan(title, 3);
            this.Children.Add(title);

            Grid contentGrid = new Grid();
            Grid.SetRow(contentGrid, 1);
            Grid.SetColumn(contentGrid, 1);

            List<string> saves = new List<string>();
            foreach (string path in Utility.FileIO.ListFiles("./saves/")) if(Path.GetExtension(path) == ".sav") saves.Add(path);

            int count = 0;
            foreach (string path in saves) {
                try {
                    SerializedGame game = new SerializedGame(Utility.FileIO.ReadData(path));
                    
                    Grid savelabel = new Grid();
                    savelabel.Background = Brushes.DarkGray;
                    savelabel.MaxHeight = 25;
                    savelabel.Margin = new Thickness(0, 0, 0, 10);
                    savelabel.VerticalAlignment = VerticalAlignment.Top;

                    int[] widths = { 3, 1, 1 };
                    for (int i = 0; i < 3; i++) {
                        ColumnDefinition def = new ColumnDefinition();
                        def.Width = new GridLength(widths[i], GridUnitType.Star);
                        savelabel.ColumnDefinitions.Add(def);
                    }

                    Label id = new Label();
                    id.Content = Path.GetFileName(path) + " (" + game.p1Name + " vs " + game.p2Name + ")";
                    Grid.SetColumn(id, 0);

                    Label noCards = new Label();
                    int num = 0;
                    foreach (SerializedGame.SerializedCard card in game.cards) {
                        if (card.isGone) num++;
                    }
                    noCards.Content = num + " / " + game.cards.Count + " Kaarten Gevonden";
                    Grid.SetColumn(noCards, 1);

                    Label time = new Label();
                    time.Content = TimeSpan.FromMilliseconds(game.gameTime).ToString("hh\\:mm\\:ss") + " Resterend";
                    Grid.SetColumn(time, 2);

                    savelabel.Children.Add(id);
                    savelabel.Children.Add(noCards);
                    savelabel.Children.Add(time);

                    DateTime last = new DateTime(1970, 1, 1);
                    savelabel.PreviewMouseLeftButtonDown += delegate (object src, MouseButtonEventArgs args) {
                        if ((DateTime.Now - last).TotalMilliseconds <= 500) {
                            onChosen(game);
                        }

                        last = DateTime.Now;
                    };


                    RowDefinition rowdef = new RowDefinition();
                    rowdef.MaxHeight = 35;

                    contentGrid.RowDefinitions.Add(rowdef);
                    Grid.SetRow(savelabel, count++);
                    contentGrid.Children.Add(savelabel);
                } catch (Exception) { }
            }

            this.Children.Add(contentGrid);

            Button returnBtn = new Button();
            returnBtn.FontSize = 22;
            returnBtn.Content = "Terug";
            returnBtn.Width = 200;
            returnBtn.Height = 50;
            returnBtn.HorizontalAlignment = HorizontalAlignment.Center;
            returnBtn.VerticalAlignment = VerticalAlignment.Center;

            returnBtn.PreviewMouseLeftButtonDown += delegate (object src, MouseButtonEventArgs args) {
                ((MainWindow) Settings.SettingsManager.Instance["Window"].Value).SetActivePage(source);
            };

            Grid.SetRow(returnBtn, 2);
            Grid.SetColumn(returnBtn, 1);

            this.Children.Add(returnBtn);
        }
    }
}
