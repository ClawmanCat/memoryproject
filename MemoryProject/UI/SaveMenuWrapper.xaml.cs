﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MemoryProject.UI {
    /// <summary>
    /// Een wrapper om het Save Menu als losse pagina te weergeven.
    /// </summary>
    public partial class SaveMenuWrapper : MemoryGamePage {
        public SaveMenuWrapper(MemoryGamePage source, SaveMenu.OnSaveChosen onChosen) {
            InitializeComponent();
            this.Content = new SaveMenu(source, onChosen);
        }
    }
}
