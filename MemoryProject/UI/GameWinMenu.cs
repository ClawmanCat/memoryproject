﻿using MemoryProject.UI.Game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace MemoryProject.UI {
    public class GameWinMenu : Grid {
        private Window window;

        public GameWinMenu(Player p1, Player p2, int numCards) {
            this.Background = (SolidColorBrush) Settings.SettingsManager.Instance["HalfGrayBrush"].Value;
            this.Height = 300;
            this.Width = 800;

            int[] widths = { 1, 5, 1 };
            foreach (int width in widths) {
                ColumnDefinition def = new ColumnDefinition();
                def.Width = new GridLength(width, GridUnitType.Star);

                this.ColumnDefinitions.Add(def);
            }

            int[] heights = { 1, 4, 1 };
            foreach (int height in heights) {
                RowDefinition def = new RowDefinition();
                def.Height = new GridLength(height, GridUnitType.Auto);

                this.RowDefinitions.Add(def);
            }


            Label title = new Label();
            title.FontSize = 64;
            title.Foreground = Brushes.DarkCyan;
            title.HorizontalAlignment = HorizontalAlignment.Center;
            title.VerticalAlignment = VerticalAlignment.Center;

            if (p1.score == p2.score) {
                title.Foreground = Brushes.Yellow;
                title.Content = "Gelijkspel!";
            } else {
                title.Foreground = Brushes.Green;
                title.Content = (p1.score > p2.score ? p1.name : p2.name) + " Wint!";
            }

            Grid.SetRow(title, 0);
            Grid.SetColumn(title, 0);
            Grid.SetColumnSpan(title, 3);
            this.Children.Add(title);


            Grid scores = new Grid();
            for (int i = 0; i < 2; i++) {
                scores.RowDefinitions.Add(new RowDefinition());
                scores.ColumnDefinitions.Add(new ColumnDefinition());

                Player p = (i == 0) ? p1 : p2;

                Label name = new Label();
                name.HorizontalAlignment = HorizontalAlignment.Center;
                name.VerticalAlignment = VerticalAlignment.Center;
                name.FontSize = 32;
                name.Content = p.name + ":";

                Label score = new Label();
                score.HorizontalAlignment = HorizontalAlignment.Center;
                score.VerticalAlignment = VerticalAlignment.Center;
                score.FontSize = 32;
                score.Content = p.score + " Punten";

                Grid.SetRow(name, i);
                Grid.SetColumn(name, 0);
                scores.Children.Add(name);

                Grid.SetRow(score, i);
                Grid.SetColumn(score, 1);
                scores.Children.Add(score);
            }

            Grid.SetRow(scores, 1);
            Grid.SetColumn(scores, 1);
            this.Children.Add(scores);


            Grid buttons = new Grid();
            int[] btnswidth = { 8, 1, 8 };
            foreach (int width in btnswidth) {
                ColumnDefinition def = new ColumnDefinition();
                def.Width = new GridLength(width, GridUnitType.Star);
                buttons.ColumnDefinitions.Add(def);
            }

            Button send = new Button();
            Button cancel = new Button();

            Button[] btns = { send, cancel };
            foreach (Button b in btns) {
                b.FontSize = 18;
                b.Padding = new Thickness(40, 7, 40, 7);
                b.HorizontalAlignment = HorizontalAlignment.Center;
                b.VerticalAlignment = VerticalAlignment.Center;
                b.Margin = new Thickness(10, 30, 10, 30);
            }

            send.Content = "Scores opslaan";
            cancel.Content = "Terug naar menu";

            send.PreviewMouseLeftButtonDown += delegate (object source, MouseButtonEventArgs args) {
                HighScores.Instance.Add(p1.name, p1.score, numCards);
                HighScores.Instance.Add(p2.name, p2.score, numCards);

                window.Close();
                ((MainWindow) Settings.SettingsManager.Instance["Window"].Value).SetActivePage(new HighScoresPage());
            };

            cancel.PreviewMouseLeftButtonDown += delegate (object source, MouseButtonEventArgs args) {
                window.Close();
            };

            Grid.SetColumn(send, 0);
            Grid.SetColumn(cancel, 2);
            buttons.Children.Add(send);
            buttons.Children.Add(cancel);

            Grid.SetRow(buttons, 2);
            Grid.SetColumn(buttons, 0);
            Grid.SetColumnSpan(buttons, 3);
            this.Children.Add(buttons);


            this.window = new Window();
            window.Content = this;
            window.Width = 800;
            window.Height = 330;
            window.ResizeMode = ResizeMode.NoResize;

            window.Closed += delegate (object source, EventArgs args) {
                ((MainWindow) Settings.SettingsManager.Instance["Window"].Value).SetActivePage(new MainMenu());
            };

            window.Topmost = true;
            window.Show();
        }
    }
}
