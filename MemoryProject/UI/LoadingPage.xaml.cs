﻿using MemoryProject.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MemoryProject.UI {
    /// <summary>
    /// Toont een laadscherm terwijl de achtergrondafbeelding aan het laden is.
    /// </summary>
    public partial class LoadingPage : MemoryGamePage {
        public LoadingPage() {
            InitializeComponent();

            Timer dotUpdater = new Timer();
            dotUpdater.Interval = 250;
            dotUpdater.AutoReset = true;

            string dots = "";
            dotUpdater.Elapsed += delegate (object source, ElapsedEventArgs args) {
                this.Dispatcher.Invoke(delegate () {
                    dots += "·";
                    LoadingDots.Content = dots;
                });

                BackgroundManager BGManager = (BackgroundManager) Settings.SettingsManager.Instance["BGManager"].Value;

                BGManager.bg.Dispatcher.Invoke(delegate () {
                    // Voorwaarden voor het laadscherm om te eindigen:
                    // - De achtergrondafbeelding moet geladen zijn.
                    // - Alle geluiden uit het huidige thema moeten geladen zijn.
                    if (BGManager.bg.ActualWidth != 0 && BGManager.bg.ActualHeight != 0 && SoundManager.AllLoaded()) {
                        dotUpdater.Stop();
                        BGManager.MakeVisible();
                        ((MainWindow) Settings.SettingsManager.Instance["Window"].Value).SetActivePage(new MainMenu());
                    }
                });
            };

            dotUpdater.Start();
        }
    }
}
