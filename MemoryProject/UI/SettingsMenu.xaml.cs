﻿using MemoryProject.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MemoryProject.UI {
    /// <summary>
    /// Scherm om de instellingen mee aan te passen.
    /// </summary>
    public partial class SettingsMenu : MemoryGamePage {
        private Dictionary<string, SettingUIElement> elements;

        public SettingsMenu() {
            InitializeComponent();

            this.elements = new Dictionary<string, SettingUIElement>();

            ContentGrid.Background = (SolidColorBrush) SettingsManager.Instance["HalfGrayBrush"].Value;

            var settings = SettingsManager.Instance.Data();
            foreach (var pair in settings) {
                if (!pair.Value.IsUserSetting) continue;

                Grid setting = new Grid();
                setting.Background = Brushes.DarkGray;
                setting.Margin = new Thickness(0, 0, 0, 10);
                setting.MaxHeight = 25;
                setting.ToolTip = pair.Value.Description;

                setting.ColumnDefinitions.Add(new ColumnDefinition());  // Naam
                setting.ColumnDefinitions.Add(new ColumnDefinition());  // Waarde

                Label name = new Label();
                name.Content = pair.Key;
                Grid.SetColumn(name, 0);
                setting.Children.Add(name);

                SettingUIElement element = SettingModifiers.Modifiers[pair.Value.ModifierID](pair.Value);
                elements.Add(pair.Key, element);

                UIElement value = element.Element;
                Grid.SetColumn(value, 1);
                setting.Children.Add(value);

                SettingsContainer.Items.Add(setting);
            }
        }


        private void OnSettingsSaved(object sender, MouseButtonEventArgs e) {
            foreach (var elem in elements) {
                if ((string) SettingsManager.Instance[elem.Key].Value != elem.Value.GetValue()) SettingsManager.Instance[elem.Key].Value = elem.Value.GetValue();
            }

            SettingsManager.Instance.ToFile();
            ((MainWindow) SettingsManager.Instance["Window"].Value).SetActivePage(new LoadingPage());
        }


        private void OnSettingsCancel(object sender, MouseButtonEventArgs e) {
            ((MainWindow) SettingsManager.Instance["Window"].Value).SetActivePage(new MainMenu());
        }
    }
}
