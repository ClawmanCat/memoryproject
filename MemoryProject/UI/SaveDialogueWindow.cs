﻿using MemoryProject.Serialization;
using MemoryProject.UI.Game;
using MemoryProject.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace MemoryProject.UI {
    public class SaveDialogueWindow {
        public SaveDialogueWindow(MemoryGame game) {
            Window input = new Window();
            input.Width = 500;
            input.Height = 200;
            input.ResizeMode = ResizeMode.NoResize;

            Grid content = new Grid();
            input.Content = content;
            for (int i = 0; i < 3; i++) content.RowDefinitions.Add(new RowDefinition());


            Label text = new Label();
            text.Content = "Nieuw spelbestand";
            text.FontSize = 24;
            text.VerticalAlignment = VerticalAlignment.Center;
            Grid.SetRow(text, 0);
            content.Children.Add(text);


            Grid inputWrapper = new Grid();
            int[] widths = { 4, 1 };
            for (int i = 0; i < 2; i++) {
                ColumnDefinition def = new ColumnDefinition();
                def.Width = new GridLength(widths[i], GridUnitType.Star);
                inputWrapper.ColumnDefinitions.Add(def);
            }

            TextBox inputBox = new TextBox();
            inputBox.Text = "memory";
            inputBox.TextWrapping = TextWrapping.NoWrap;
            inputBox.AcceptsReturn = false;
            inputBox.VerticalAlignment = VerticalAlignment.Center;
            Grid.SetColumn(inputBox, 0);

            Label extension = new Label();
            extension.Content = ".sav";
            extension.VerticalAlignment = VerticalAlignment.Center;
            Grid.SetColumn(extension, 1);

            inputWrapper.Children.Add(inputBox);
            inputWrapper.Children.Add(extension);

            Grid.SetRow(inputWrapper, 1);
            content.Children.Add(inputWrapper);


            Grid buttons = new Grid();
            for (int i = 0; i < 2; i++) buttons.ColumnDefinitions.Add(new ColumnDefinition());

            Button cancel = new Button();
            cancel.Content = "Annuleren";
            cancel.Margin = new Thickness(10);

            cancel.PreviewMouseLeftButtonDown += delegate (object source, MouseButtonEventArgs args) {
                input.Close();
            };

            Grid.SetColumn(cancel, 0);

            Button save = new Button();
            save.Content = "Opslaan";
            save.Margin = new Thickness(10);

            save.PreviewMouseLeftButtonDown += delegate (object source, MouseButtonEventArgs args) {
                SerializedGame serialized = game.Serialize();
                try {
                    FileIO.WriteData("./saves/" + inputBox.Text + ".sav", serialized.array);
                    MessageBox.Show("Spel opgeslagen als " + inputBox.Text + ".sav");
                    input.Close();
                } catch (Exception e) {
                    MessageBox.Show("Er is een fout opgetreden: " + e.Message);
                }
            };

            Grid.SetColumn(cancel, 1);

            buttons.Children.Add(cancel);
            buttons.Children.Add(save);

            Grid.SetRow(buttons, 2);
            content.Children.Add(buttons);


            input.Topmost = true;
            input.Show();
        }
    }
}
