﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace MemoryProject.UI.Game {
    /// <summary>
    /// De gametimer houdt de tijd van het spel en de huidige beurt bij, en zorgt dat ze op tijd worden geeindigt.
    /// </summary>
    public class GameTimer {
        public delegate void OnTurnEnd();
        public delegate void OnGameEnd();

        private double turnLength, gameLength;
        private double actualTurnLength, actualGameLength;
        private double pausedTurnLeft, pausedGameLeft;
        private DateTime turnStarted, gameStarted;
        private Timer turnTimer, gameTimer;
        private bool isPaused;
        private OnTurnEnd onTurnEnd;
        private OnGameEnd onGameEnd;

        /// <summary>
        /// Maak een nieuwe timer.
        /// </summary>
        /// <param name="turnLength">De lengte van een beurt.</param>
        /// <param name="gameLength">De lengte van het spel.</param>
        public GameTimer(double turnLength, double gameLength, OnTurnEnd onTurn, OnGameEnd onGame) {
            this.turnLength = turnLength;
            this.gameLength = gameLength;
            this.actualTurnLength = turnLength;
            this.actualGameLength = gameLength;

            this.isPaused = false;

            this.onTurnEnd = onTurn;
            this.onGameEnd = onGame;

            turnTimer = new Timer();
            turnTimer.Interval = turnLength;
            turnTimer.Elapsed += delegate (object source, ElapsedEventArgs args) { EndTurn(); };

            gameTimer = new Timer();
            gameTimer.Interval = gameLength;
            gameTimer.Elapsed += delegate (object source, ElapsedEventArgs args) { EndGame(); };

            turnStarted = DateTime.Now;
            gameStarted = DateTime.Now;

            turnTimer.Start();
            gameTimer.Start();
        }

        public GameTimer(double turnLength, double gameLeft, double turnLeft, OnTurnEnd onTurn, OnGameEnd onGame) : this(turnLength, gameLeft, onTurn, onGame) {
            Pause();
            this.pausedTurnLeft = turnLeft;
            Resume();
        }

        public void EndTurn() {
            turnTimer.Stop();

            actualTurnLength = turnLength;
            turnStarted = DateTime.Now;
            onTurnEnd();

            turnTimer.Start();
        }

        public void EndGame() {
            turnTimer.Stop();
            gameTimer.Stop();

            onGameEnd();
        }

        public void Pause() {
            if (isPaused) return;

            turnTimer.Stop();
            gameTimer.Stop();

            this.pausedTurnLeft = this.actualTurnLength - (DateTime.Now - turnStarted).TotalMilliseconds;
            this.pausedGameLeft = this.actualGameLength - (DateTime.Now - gameStarted).TotalMilliseconds;

            isPaused = true;
        }

        public void Resume() {
            if (!isPaused) return;

            ElapsedEventHandler handler = null;
            handler = delegate (object source, ElapsedEventArgs args) {
                turnTimer.Stop();
                this.actualTurnLength = turnLength;
                turnTimer.Interval = turnLength;
                turnTimer.Elapsed -= handler;
                turnTimer.Start();
            };

            actualGameLength = pausedGameLeft;
            actualTurnLength = pausedTurnLeft;

            gameTimer.Interval = pausedGameLeft;
            turnTimer.Interval = pausedTurnLeft;

            turnTimer.Elapsed += handler;

            gameStarted = DateTime.Now;
            turnStarted = DateTime.Now;

            gameTimer.Start();
            turnTimer.Start();

            isPaused = false;
        }

        public double GetGameTimeRemaining() { return isPaused ? pausedGameLeft : actualGameLength - (DateTime.Now - gameStarted).TotalMilliseconds; }
        public double GetTurnTimeRemaining() { return isPaused ? pausedTurnLeft : actualTurnLength - (DateTime.Now - turnStarted).TotalMilliseconds; }
    }
}
