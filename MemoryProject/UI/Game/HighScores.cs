﻿using MemoryProject.Serialization;
using MemoryProject.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MemoryProject.UI.Game {
    public class HighScores {
        public class HighScore {
            public string name;
            public int numCards;
            public int score;

            public HighScore(string name, int numCards, int score) {
                this.name = name;
                this.numCards = numCards;
                this.score = score;
            }
        }

        public static HighScores Instance = new HighScores();
        private List<HighScore> scores;

        private HighScores() {
            this.scores = new List<HighScore>();

            FromFile();
        }


        ~HighScores() {
            ToFile();
        }


        public void Add(string name, int score, int numCards) {
            this.scores.Add(new HighScore(name, numCards, score));
        }


        public IReadOnlyList<HighScore> Data() {
            return scores;
        }


        public void ToFile() {
            List<SerializedHighScores.SerializedHighScore> serialized = new List<SerializedHighScores.SerializedHighScore>();
            foreach (HighScore score in scores) serialized.Add(new SerializedHighScores.SerializedHighScore(score.name, score.numCards, score.score));

            SerializedHighScores serializedHighScores = new SerializedHighScores(serialized);
            FileIO.WriteData("./scores.dat", serializedHighScores.array);
        }


        public void FromFile() {
            if (!FileIO.Exists("./scores.dat")) return;

            SerializedHighScores serialized = new SerializedHighScores(FileIO.ReadData("./scores.dat"));
            foreach (SerializedHighScores.SerializedHighScore score in serialized.scores) scores.Add(new HighScore(score.name, score.numCards, score.score));
        }
    }
}
