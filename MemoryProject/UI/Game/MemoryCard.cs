﻿using MemoryProject.Serialization;
using MemoryProject.UI.Animation;
using MemoryProject.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;
using static MemoryProject.UI.Animation.CardAnimation;

namespace MemoryProject.UI.Game {
    public class MemoryCard : Label {
        public delegate void OnCardClicked(MemoryCard card);

        private MemoryGrid container;
        private GridPos pos;
        private Image front, back;
        private Image frontSource, backSource;
        private bool isFlipped, isAnimating;
        private OnCardClicked listener;
        private int id;
        private string img;

        public MemoryCard(MemoryGrid container, GridPos pos, Image front, Image back, OnCardClicked listener, int id) : base() {
            this.container = container;
            this.pos = pos;
            this.frontSource = front;
            this.backSource = back;
            this.listener = listener;
            this.id = id;

            this.img = id.ToString();
            while (this.img.Length < 3) this.img = '0' + this.img;

            this.front = new Image();
            this.front.Source = this.frontSource.Source;
            this.back = new Image();
            this.back.Source = this.backSource.Source;

            this.isFlipped = false;
            this.isAnimating = false;

            this.PreviewMouseLeftButtonDown += delegate (object sender, MouseButtonEventArgs args) {
                if (isClickable()) listener(this);
            };

            update();
        }


        public MemoryCard(MemoryGrid container, SerializedGame.SerializedCard card, Image front, Image back, OnCardClicked listener, bool isFlipped) {
            this.container = container;
            this.pos = new GridPos(card.row, card.col);
            this.frontSource = front;
            this.backSource = back;
            this.listener = listener;
            this.id = card.id;
            this.img = card.img;

            this.front = new Image();
            this.front.Source = this.frontSource.Source;
            this.back = new Image();
            this.back.Source = this.backSource.Source;

            this.isFlipped = false;
            this.isAnimating = false;

            this.PreviewMouseLeftButtonDown += delegate (object sender, MouseButtonEventArgs args) {
                if (isClickable()) listener(this);
            };

            update();
            if (isFlipped) this.flip(null);
            if (card.isGone) this.remove(null);
        }


        /// <summary>
        /// Update de getoonde afbeelding zodat deze gelijk is aan de kant van de kaart die getoond is.
        /// </summary>
        private void update() {
            this.Content = getShownImage();
        }

        /// <summary>
        /// Draai de kaart om.
        /// </summary>
        /// <param name="animate">Of het draaiden van de kaart geanimeerd moet worden.</param>
        public void flip(OnAnimationDone onDone, bool animate = true) {
            if (animate) {
                if (!isAnimating) CardAnimation.FlipCard(this, onDone);
            } else {
                this.isFlipped = !this.isFlipped;
                update();
            }
        }

        /// <summary>
        /// Verwijder de kaart van het speelveld.
        /// </summary>
        /// <param name="animate">Of het verdwijnen van de kaart geanimeerd moet worden.</param>
        public void remove(OnAnimationDone onDone, bool animate = true) {
            if (animate) {
                if (!isAnimating) CardAnimation.EraseCard(this, onDone);
            } else {
                container.Children.Remove(this);
            }
        }

        /// <summary>
        /// Laat deze kaart weten dat zijn positie verandert is.
        /// DEZE FUNCTIE VERANDERT DE POSITIE VAN DE KAART IN HET GRID NIET.
        /// </summary>
        /// <param name="pos">De nieuwe positie van de kaart.</param>
        public void move(GridPos pos) {
            this.pos = pos;
        }

        public void setAnimated(bool animated) { this.isAnimating = animated; }

        public GridPos getPosition()  { return pos; }
        public bool isClickable()     { return !isFlipped && !isAnimating; }
        public bool getIsFlipped()    { return isFlipped; }
        public bool getIsAnimating()  { return isAnimating; }
        public int getID()            { return id; }
        public Image getFrontImage()  { return front; }
        public Image getBackImage()   { return back;  }
        public Image getShownImage()  { return (isFlipped ? back : front); }
        public Image getHiddenImage() { return (isFlipped ? front : back); }
        public Image getFrontImageSource()  { return frontSource; }
        public Image getBackImageSource()   { return backSource;  }
        public Image getShownImageSource()  { return (isFlipped ? backSource : frontSource); }
        public Image getHiddenImageSource() { return (isFlipped ? frontSource : backSource); }

        public SerializedGame.SerializedCard Serialize() {
            return new SerializedGame.SerializedCard(pos.row, pos.col, id, img, !container.Children.Contains(this));
        }
    }
}
