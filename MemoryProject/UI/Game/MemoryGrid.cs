﻿using MemoryProject.Serialization;
using MemoryProject.UI.Animation;
using MemoryProject.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace MemoryProject.UI.Game {
    /// <summary>
    /// Een MemoryGrid is een Grid dat MemoryCards bevat.
    /// </summary>
    public class MemoryGrid : Grid {
        private List<MemoryCard> cards;         // De kaarten van linksboven naar rechtsonder.
        private MemoryCard[,] cardPositions;    // De zelfde kaarten, gesorteerd op rij en kolom.
        private MemoryCard flippedCard;         // De kaart die op dit moment is omgedraaid, anders null.
        private string theme;                   // Het huidige thema.
        private int numCards;                   // De hoeveelheid kaarten in het spel. (reeds gevonden kaarten veranderen deze waarde niet.)
        private GridPos size;                   // De afmetingen van de kaartenmatrix.
        private bool isLocked;                  // Als het grid locked is kunnen er geen kaarten worden omgedraaid.
        private Player player1, player2;        // De spelers.
        private Player currentPlayer;           // De speler die aan de beurt is.
        private bool ended = false;

        public GameTimer timer;

        /// <summary>
        /// Laadt een nieuw grid gebruikmakende van het huidige thema.
        /// </summary>
        public MemoryGrid(Player p1, Player p2) {
            this.theme = (string) Settings.SettingsManager.Instance["Thema"].Value;
            this.flippedCard = null;
            this.isLocked = true;

            this.player1 = p1;
            this.player2 = p2;
            this.currentPlayer = p1;

            // Tel de hoeveelheid kaarten en laadt de kaartafbeeldingen. (Elke kaart komt twee keer voor.)
            List<ImageSource> cardBacks = new List<ImageSource>();
            ImageSource cardFront = FileIO.ReadImage("./" + theme + "/front.png");

            this.numCards = 0;
            bool exists = true;
            while (exists) {
                string id = (numCards / 2).ToString();
                while (id.Length < 3) id = '0' + id;

                if (FileIO.Exists("./" + theme + "/" + id + ".png")) {
                    cardBacks.Add(FileIO.ReadImage("./" + theme + "/" + id + ".png"));
                    numCards += 2;
                } else {
                    exists = false;
                }
            }

            // Maak de MemoryCards aan.
            this.size = calculateBestGridDimensions();
            this.cards = new List<MemoryCard>();
            this.cardPositions = new MemoryCard[size.row, size.col];

            for (int i = 0; i < size.row; i++) this.RowDefinitions.Add(new RowDefinition());
            for (int i = 0; i < size.col; i++) this.ColumnDefinitions.Add(new ColumnDefinition());

            int index = 0;
            for (int r = 0; r < size.row; r++) {
                for (int c = 0; c < size.col; c += 2) {
                    for (int i = 0; i < 2; i++) {
                        Image front = new Image();
                        front.Source = cardFront;
                        Image back = new Image();
                        back.Source = cardBacks[index];

                        MemoryCard card = new MemoryCard(this, new GridPos(r, c + i), front, back, onCardClicked, index);
                        card.HorizontalAlignment = HorizontalAlignment.Center;
                        card.VerticalAlignment = VerticalAlignment.Center;

                        cards.Add(card);
                        cardPositions[r, c + i] = card;

                        Grid.SetRow(card, r);
                        Grid.SetColumn(card, c + i);
                        this.Children.Add(card);
                    }

                    index++;
                }
            }

            Shuffle();

            int turntime = 10000;
            int gametime = numCards * numCards * 1000;
            int num_turns = (gametime / turntime) + 1;
            gametime = num_turns * turntime;

            this.timer = new GameTimer(
                turntime,
                gametime,
                
                delegate () {
                    if (!this.isLocked) {
                        if (flippedCard != null) {
                            if (flippedCard.getIsAnimating()) {
                                // De kaart beweegt nog. Wacht eerst tot hij klaar is.

                                Timer t = new Timer();
                                t.Interval = 10;

                                t.Elapsed += delegate (object sender, ElapsedEventArgs args) {
                                    if (flippedCard == null) {
                                        t.Stop();
                                    } else {
                                        if (!flippedCard.getIsAnimating()) {
                                            flippedCard.flip(null);
                                            t.Stop();
                                        }
                                    }
                                };

                                t.Start();
                            } else {
                                flippedCard.flip(null);
                            }
                        }

                        currentPlayer = (currentPlayer == player1 ? player2 : player1);
                    }
                },

                delegate () { OnGameDone(); }
            );

            this.timer.Pause();
        }

        // TODO: Merge gezamelijke functionaliteit van constructors.
        public MemoryGrid(SerializedGame savefile, Player p1, Player p2) {
            this.theme = savefile.theme;
            Settings.SettingsManager.Instance["Thema"].Value = this.theme;

            this.flippedCard = null;
            this.isLocked = true;


            this.player1 = p1;
            this.player2 = p2;
            this.currentPlayer = savefile.isP1turn ? p1 : p2;

            this.numCards = savefile.cards.Count;

            // Maak de MemoryCards aan.
            this.size = calculateBestGridDimensions();
            this.cards = new List<MemoryCard>();
            this.cardPositions = new MemoryCard[size.row, size.col];

            for (int i = 0; i < size.row; i++) this.RowDefinitions.Add(new RowDefinition());
            for (int i = 0; i < size.col; i++) this.ColumnDefinitions.Add(new ColumnDefinition());

            ImageSource frontSrc = FileIO.ReadImage("./" + theme + "/front.png");

            int index = 0;
            for (int r = 0; r < size.row; r++) {
                for (int c = 0; c < size.col; c++) {
                    SerializedGame.SerializedCard savecard = savefile.cards[index];

                    Image front = new Image();
                    front.Source = frontSrc;
                    Image back = new Image();
                    back.Source = FileIO.ReadImage("./" + theme + "/" + savecard.img + ".png");

                    bool isThisCardFlipped = (savefile.flippedRow == savecard.row && savefile.flippedCol == savecard.col);
                    MemoryCard card = new MemoryCard(this, savecard, front, back, onCardClicked, isThisCardFlipped);
                    card.HorizontalAlignment = HorizontalAlignment.Center;
                    card.VerticalAlignment = VerticalAlignment.Center;

                    cards.Add(card);
                    cardPositions[savecard.row, savecard.col] = card;

                    Grid.SetRow(card, savecard.row);
                    Grid.SetColumn(card, savecard.col);
                    this.Children.Add(card);

                    index++;
                }
            }

            this.timer = new GameTimer(
                10000,
                savefile.gameTime,
                savefile.turnTime,

                delegate () {
                    if (!this.isLocked) {
                        if (flippedCard != null) {
                            if (flippedCard.getIsAnimating()) {
                                // De kaart beweegt nog. Wacht eerst tot hij klaar is.

                                Timer t = new Timer();
                                t.Interval = 10;

                                t.Elapsed += delegate (object sender, ElapsedEventArgs args) {
                                    if (flippedCard == null) {
                                        t.Stop();
                                    } else {
                                        if (!flippedCard.getIsAnimating()) {
                                            flippedCard.flip(null);
                                            t.Stop();
                                        }
                                    }
                                };

                                t.Start();
                            } else {
                                flippedCard.flip(null);
                            }
                        }

                        currentPlayer = (currentPlayer == player1 ? player2 : player1);
                    }
                },

                delegate () { OnGameDone(); }
            );

            this.timer.Pause();
        }


        public void Start() {
            if (!this.isLocked) return;

            this.isLocked = false;
            this.timer.Resume();
        }


        /// <summary>
        /// Draait alle kaarten. Alleen voor testdoeleinden.
        /// </summary>
        public void ShowAll() {
            foreach (MemoryCard card in this.cards) card.flip(null);
        }


        /// <summary>
        /// Verander de afmetingen van de kaartenmatrix als de afmetingen van het grid veranderen.
        /// </summary>
        /// <param name="sizeInfo">Niet gebruikt door deze functie</param>
        //protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo) {
        //    GridPos best = calculateBestGridDimensions();
        //    if (best.row != size.row || best.col != size.col) resize(best);
        //
        //    base.OnRenderSizeChanged(sizeInfo);
        //}


        /// <summary>
        /// Randomiseer de volgorde van de kaarten.
        /// </summary>
        private void Shuffle() {
            this.cards.Shuffle();

            int index = 0;
            for (int r = 0; r < size.row; r++) {
                for (int c = 0; c < size.col; c++) {
                    MemoryCard card = cards[index];

                    cardPositions[r, c] = card;
                    card.move(new GridPos(r, c));

                    this.Children.Remove(card);
                    Grid.SetRow(card, r);
                    Grid.SetColumn(card, c);
                    this.Children.Add(card);

                    index++;
                }
            }
        }


        /// <summary>
        /// Bereken de beste afmetingen voor de kaartenmatrix, gebruikmakende van de aspect ratio van het gridelement.
        /// </summary>
        /// <returns>De kaartconfiguratie die de aspect ratio van het grid het dichtst benadert.</returns>
        private GridPos calculateBestGridDimensions() {
            double aspect = 1;
            if (this.ActualWidth != 0 && this.ActualHeight != 0) aspect = this.ActualWidth / this.ActualHeight;

            List<GridPos> divisors = new List<GridPos>();
            for (int i = 1; i < numCards; i++) if (numCards % i == 0) divisors.Add(new GridPos(i, numCards / i));


            double CalcAspectDistance(GridPos pos) {
                return Math.Abs(aspect - (pos.col / pos.row));
            }

            double closest = CalcAspectDistance(divisors[0]);
            int closestIndex = 0;

            for (int i = 1; i < divisors.Count; i++) {
                double dist = CalcAspectDistance(divisors[i]);
                if (dist < closest) {
                    closest = dist;
                    closestIndex = i;
                }
            }

            return divisors[closestIndex];
        }


        /// <summary>
        /// Verander de afmetingen van de kaartenmatrix.
        /// </summary>
        /// <param name="size">De nieuwe afmetingen van de kaartenmatrix.</param>
        private void resize(GridPos size) {
            cardPositions = new MemoryCard[size.row, size.col];

            this.Children.Clear();
            this.RowDefinitions.Clear();
            this.ColumnDefinitions.Clear();

            for (int i = 0; i < size.row; i++) this.RowDefinitions.Add(new RowDefinition());
            for (int i = 0; i < size.col; i++) this.ColumnDefinitions.Add(new ColumnDefinition());

            int index = 0;
            for (int r = 0; r < size.row; r++) {
                for (int c = 0; c < size.col; c++) {
                    MemoryCard card = cards[index++];
                    card.move(new GridPos(r, c));
                    cardPositions[r, c] = card;
                    
                    Grid.SetRow(card, r);
                    Grid.SetColumn(card, c);
                    this.Children.Add(card);
                }
            }
        }


        public void OnGameDone(bool showMenu = true) {
            if (ended) return;
            else ended = true;

            this.isLocked = true;
            timer.Pause();

            if (showMenu) {
                Application.Current.Dispatcher.Invoke(delegate () {
                    SoundManager.PlaySound("victory");
                    GameWinMenu menu = new GameWinMenu(player1, player2, numCards);
                });
            }
        }


        /// <summary>
        /// Wordt aangeroepen door iedere kaart die wordt omgedraaid.
        /// Bevat het grootste deel van de spellogica.
        /// </summary>
        /// <param name="card">De gedraaide kaart.</param>
        private void onCardClicked(MemoryCard card) {
            if (isLocked) return;

            if (flippedCard == null) {
                SoundManager.PlaySound("select");

                // Er is nog geen omgedraaide kaart, draai deze kaart om.
                flippedCard = card;
                card.flip(null);
            } else {
                // Er is al een omgedraaide kaart, vergelijk de twee kaarten.
                card.flip(null);
                SoundManager.PlaySound("select");

                if (flippedCard.getID() == card.getID()) {
                    // De twee kaarten zijn gelijk, verwijder ze.
                    this.isLocked = true;
                    bool flippedDone = false, cardDone = false;
                    currentPlayer.score += 100;

                    CardShowTimer timer = new CardShowTimer(delegate () {
                        void OnCardDone(bool isFlippedCard) {
                            this.Dispatcher.Invoke(delegate () {
                                if (isFlippedCard) flippedDone = true;
                                else cardDone = true;

                                if (flippedDone && cardDone) {
                                    this.isLocked = false;
                                    flippedCard = null;

                                    // 2 keer want we willen dat de timer reset maar niet dat de speler verandert. 
                                    // TODO: Doe dit op een logischere manier.
                                    this.timer.EndTurn();
                                    this.timer.EndTurn();   
                                }

                                if (this.Children.Count == 0) OnGameDone();
                            });
                        }

                        flippedCard.remove(delegate () { OnCardDone(true); });
                        card.remove(delegate () { OnCardDone(false); });
                    });
                } else {
                    // De twee kaarten verschillen, draai ze terug.
                    this.isLocked = true;
                    bool flippedDone = false, cardDone = false;

                    CardShowTimer timer = new CardShowTimer(delegate () {
                        void OnCardDone(bool isFlippedCard) {
                            this.Dispatcher.Invoke(delegate () {
                                if (isFlippedCard) flippedDone = true;
                                else cardDone = true;

                                if (flippedDone && cardDone) {
                                    this.isLocked = false;
                                    flippedCard = null;
                                    this.timer.EndTurn();
                                }

                                SoundManager.PlaySound("lost");
                            });
                        }

                        flippedCard.flip(delegate () { OnCardDone(true); });
                        card.flip(delegate () { OnCardDone(false); });
                    });
                }
            }
        }

        public Player getActivePlayer() { return currentPlayer; }

        public SerializedGame Serialize() {
            List<SerializedGame.SerializedCard> cards = new List<SerializedGame.SerializedCard>();
            foreach (MemoryCard card in this.cards) cards.Add(card.Serialize());

            return new SerializedGame(
                this.theme,
                player1.name,
                player2.name,
                player1.score,
                player2.score,
                (currentPlayer == player1),
                timer.GetTurnTimeRemaining(),
                timer.GetGameTimeRemaining(),
                (flippedCard == null ? -1 : flippedCard.getPosition().row),
                (flippedCard == null ? -1 : flippedCard.getPosition().col),
                cards
            );
        }
    }
}
