﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MemoryProject.UI.Game {
    public class Player {
        public readonly string name;
        public int score;

        public Player(string name, int score = 0) {
            this.name = name;
            this.score = score;
        }
    }
}
