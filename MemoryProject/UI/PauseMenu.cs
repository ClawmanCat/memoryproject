﻿using MemoryProject.Serialization;
using MemoryProject.UI;
using MemoryProject.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace MemoryProject {
    class PauseMenu : Grid {
        private MemoryGame game;
        private delegate void onItemClicked();

        public PauseMenu(MemoryGame game) : base() {
            this.game = game;

            SolidColorBrush bg = (SolidColorBrush) Settings.SettingsManager.Instance["HalfGrayBrush"].Value;

            this.Background = bg;
            this.Margin = new Thickness(100, 100, 100, 100);

            List<Tuple<string, onItemClicked>> items = new List<Tuple<string, onItemClicked>>{
                new Tuple<string, onItemClicked>("Terug naar hoofdmenu", delegate () { ((MainWindow) Window.GetWindow(this)).SetActivePage(new MainMenu());   }),
                new Tuple<string, onItemClicked>("Spel herstarten",      delegate () { ((MainWindow) Window.GetWindow(this)).SetActivePage(new MemoryGame()); }),
                new Tuple<string, onItemClicked>("Spel Opslaan",         delegate () { new SaveDialogueWindow(this.game); }),
                new Tuple<string, onItemClicked>("Terug naar het spel",  delegate () { game.TogglePause(); })
            };

            ColumnDefinition centerCol = new ColumnDefinition();
            centerCol.Width = new GridLength(4, GridUnitType.Star);

            RowDefinition centerRow = new RowDefinition();
            centerRow.Height = new GridLength(4, GridUnitType.Star);

            this.ColumnDefinitions.Add(new ColumnDefinition());
            this.ColumnDefinitions.Add(centerCol);
            this.ColumnDefinitions.Add(new ColumnDefinition());

            this.RowDefinitions.Add(new RowDefinition());
            this.RowDefinitions.Add(centerRow);
            this.RowDefinitions.Add(new RowDefinition());

            TextBlock title = new TextBlock();
            title.Text = "Gepauzeerd";
            title.Foreground = Brushes.DarkCyan;
            title.FontSize = 64;
            title.HorizontalAlignment = HorizontalAlignment.Center;
            title.VerticalAlignment = VerticalAlignment.Center;
            Grid.SetColumnSpan(title, 3);
            this.Children.Add(title);

            Grid contentGrid = new Grid();
            Grid.SetRow(contentGrid, 1);
            Grid.SetColumn(contentGrid, 1);

            for (int i = 0; i < items.Count; i++) {
                var pair = items[i];

                Button btn = new Button();
                btn.Content = pair.Item1;
                btn.Margin = new Thickness(10, 10, 10, 10);
                btn.PreviewMouseLeftButtonDown += delegate (object source, MouseButtonEventArgs args) { pair.Item2(); };
                btn.FontSize = 22;

                contentGrid.RowDefinitions.Add(new RowDefinition());
                Grid.SetRow(btn, i);
                contentGrid.Children.Add(btn);
            }

            this.Children.Add(contentGrid);
        }
    }
}
