﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MemoryProject.Serialization {
    /// <summary>
    /// Zorgt voor de serialisatie van de SettingsManager.
    /// </summary>
    public class SerializedSettings {
        public class SerializedSetting {
            public readonly string id, setter, value, description;
            public readonly byte[] array;

            public SerializedSetting(string id, string setter, string value, string description) {
                this.id = id;
                this.setter = setter;
                this.value = value;
                this.description = description;

                this.array = SerializationUtils.MergeArrays(
                    this.id.ToByteArray(),
                    this.setter.ToByteArray(),
                    this.value.ToByteArray(),
                    this.description.ToByteArray()
                );
            }

            public SerializedSetting(byte[] array) {
                this.array = array;

                byte[][] split   = SerializationUtils.SplitArray(array);
                this.id          = SerializationUtils.StringFromByteArray(split[0]);
                this.setter      = SerializationUtils.StringFromByteArray(split[1]);
                this.value       = SerializationUtils.StringFromByteArray(split[2]);
                this.description = SerializationUtils.StringFromByteArray(split[3]);
            }
        }


        public IReadOnlyList<SerializedSetting> settings;
        public readonly byte[] array;

        public SerializedSettings(List<SerializedSetting> settings) {
            this.settings = settings;

            byte[][] arrays = new byte[settings.Count][];
            for (int i = 0; i < settings.Count; i++) arrays[i] = settings[i].array;

            this.array = SerializationUtils.MergeArrays(arrays);
        }

        public SerializedSettings(byte[] array) {
            this.array = array;
            this.settings = new List<SerializedSetting>();

            byte[][] split = SerializationUtils.SplitArray(array);
            foreach (byte[] arr in split) ((List<SerializedSetting>) this.settings).Add(new SerializedSetting(arr));
        }
    }
}
