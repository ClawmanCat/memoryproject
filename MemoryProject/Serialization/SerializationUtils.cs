﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MemoryProject.Serialization {
    public static class SerializationUtils {
        public static byte[] MergeArrays(params byte[][] arrays) {
            long size = 4;
            foreach (byte[] arr in arrays) size += (arr.Length + 4);

            long index = 0;
            byte[] merged = new byte[size];
            arrays.Length.ToByteArray().CopyTo(merged, index);
            index += 4;

            foreach (byte[] arr in arrays) {
                arr.Length.ToByteArray().CopyTo(merged, index);
                index += 4;
            }

            foreach (byte[] arr in arrays) {
                arr.CopyTo(merged, index);
                index += arr.Length;
            }

            return merged;
        }


        public static byte[][] SplitArray(byte[] array) {
            int numElems = IntFromByteArray(array);

            int[] lengths = new int[numElems];
            for (int i = 0; i < numElems; i++) {
                lengths[i] = BitConverter.ToInt32(array, (i * 4) + 4);
            }

            byte[][] contents = new byte[numElems][];
            int start = (4 * numElems) + 4;
            for (int i = 0; i < numElems; i++) {
                contents[i] = new byte[lengths[i]];
                Array.ConstrainedCopy(array, start, contents[i], 0, lengths[i]);

                start += lengths[i];
            }

            return contents;
        }


        public static byte[] ToByteArray(this string obj) {
            return Encoding.ASCII.GetBytes(obj);
        }


        public static byte[] ToByteArray(this bool obj) {
            return BitConverter.GetBytes(obj);
        }


        public static byte[] ToByteArray(this int obj) {
            return BitConverter.GetBytes(obj);
        }


        public static byte[] ToByteArray(this double obj) {
            return BitConverter.GetBytes(obj);
        }


        public static string StringFromByteArray(this byte[] array) {
            return Encoding.ASCII.GetString(array);
        }


        public static bool BoolFromByteArray(byte[] array) {
            return BitConverter.ToBoolean(array, 0);
        }


        public static int IntFromByteArray(byte[] array) {
            return BitConverter.ToInt32(array, 0);
        }


        public static double DoubleFromByteArray(byte[] array) {
            return BitConverter.ToDouble(array, 0);
        }
    }
}
