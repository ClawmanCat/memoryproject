﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MemoryProject.Serialization {
    public class SerializedGame {
        public class SerializedCard {
            public readonly int row, col;
            public readonly int id;
            public readonly string img;
            public readonly byte[] array;
            public readonly bool isGone;

            public SerializedCard(int row, int col, int id, string img, bool isGone) {
                this.row = row;
                this.col = col;
                this.id = id;
                this.img = img;
                this.isGone = isGone;

                this.array = SerializationUtils.MergeArrays(
                    row.ToByteArray(),
                    col.ToByteArray(),
                    id.ToByteArray(),
                    img.ToByteArray(),
                    isGone.ToByteArray()
                );
            }

            public SerializedCard(byte[] array) {
                this.array = array;

                byte[][] split = SerializationUtils.SplitArray(array);
                this.row = SerializationUtils.IntFromByteArray(split[0]);
                this.col = SerializationUtils.IntFromByteArray(split[1]);
                this.id = SerializationUtils.IntFromByteArray(split[2]);
                this.img = SerializationUtils.StringFromByteArray(split[3]);
                this.isGone = SerializationUtils.BoolFromByteArray(split[4]);
            }
        }


        public readonly string theme;
        public readonly string p1Name, p2Name;
        public readonly int p1Score, p2Score;
        public readonly bool isP1turn;
        public readonly double turnTime, gameTime;
        public readonly int flippedRow, flippedCol;
        public IReadOnlyList<SerializedCard> cards;

        public readonly byte[] array;

        public SerializedGame(string theme, string p1name, string p2name, int p1score, int p2score, bool isP1Turn, double turnTime, double gameTime, int flippedRow, int flippedCol, List<SerializedCard> cards) {
            this.theme = theme;
            this.p1Name = p1name;
            this.p2Name = p2name;
            this.p1Score = p1score;
            this.p2Score = p2score;
            this.isP1turn = isP1Turn;
            this.turnTime = turnTime;
            this.gameTime = gameTime;
            this.flippedRow = flippedRow;
            this.flippedCol = flippedCol;
            this.cards = cards;

            byte[][] cardArrays = new byte[cards.Count][];
            for (int i = 0; i < cards.Count; i++) cardArrays[i] = cards[i].array;

            this.array = SerializationUtils.MergeArrays(
                theme.ToByteArray(),
                p1name.ToByteArray(),
                p2name.ToByteArray(),
                p1score.ToByteArray(),
                p2score.ToByteArray(),
                isP1turn.ToByteArray(),
                turnTime.ToByteArray(),
                gameTime.ToByteArray(),
                flippedRow.ToByteArray(),
                flippedCol.ToByteArray(),
                SerializationUtils.MergeArrays(cardArrays)
            );
        }

        public SerializedGame(byte[] array) {
            this.array = array;
            this.cards = new List<SerializedCard>();

            byte[][] split  = SerializationUtils.SplitArray(array);
            this.theme      = SerializationUtils.StringFromByteArray(split[0]);
            this.p1Name     = SerializationUtils.StringFromByteArray(split[1]);
            this.p2Name     = SerializationUtils.StringFromByteArray(split[2]);
            this.p1Score    = SerializationUtils.IntFromByteArray   (split[3]);
            this.p2Score    = SerializationUtils.IntFromByteArray   (split[4]);
            this.isP1turn   = SerializationUtils.BoolFromByteArray  (split[5]);
            this.turnTime   = SerializationUtils.DoubleFromByteArray(split[6]);
            this.gameTime   = SerializationUtils.DoubleFromByteArray(split[7]);
            this.flippedRow = SerializationUtils.IntFromByteArray   (split[8]);
            this.flippedCol = SerializationUtils.IntFromByteArray   (split[9]);

            byte[][] cardArrays = SerializationUtils.SplitArray(split[10]);
            foreach (byte[] arr in cardArrays) ((List<SerializedCard>) cards).Add(new SerializedCard(arr));
        }
    }
}
