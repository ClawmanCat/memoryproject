﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MemoryProject.Serialization {
    public class SerializedHighScores {
        public class SerializedHighScore {
            public readonly string name;
            public readonly int numCards;
            public readonly int score;

            public readonly byte[] array;

            public SerializedHighScore(string name, int numCards, int score) {
                this.name = name;
                this.numCards = numCards;
                this.score = score;

                this.array = SerializationUtils.MergeArrays(
                    name.ToByteArray(),
                    numCards.ToByteArray(),
                    score.ToByteArray()
                );
            }

            public SerializedHighScore(byte[] array) {
                this.array = array;

                byte[][] split = SerializationUtils.SplitArray(array);
                this.name     = SerializationUtils.StringFromByteArray(split[0]);
                this.numCards = SerializationUtils.IntFromByteArray(split[1]);
                this.score    = SerializationUtils.IntFromByteArray(split[2]);
            }
        }

        public IReadOnlyList<SerializedHighScore> scores;
        public readonly byte[] array;

        public SerializedHighScores(List<SerializedHighScore> scores) {
            this.scores = scores;

            byte[][] arrays = new byte[scores.Count][];
            for (int i = 0; i < scores.Count; i++) arrays[i] = scores[i].array;

            this.array = SerializationUtils.MergeArrays(arrays);
        }

        public SerializedHighScores(byte[] array) {
            this.array = array;
            this.scores = new List<SerializedHighScore>();

            byte[][] split = SerializationUtils.SplitArray(array);
            foreach (byte[] arr in split) ((List<SerializedHighScore>) scores).Add(new SerializedHighScore(arr));
        }
    }
}
