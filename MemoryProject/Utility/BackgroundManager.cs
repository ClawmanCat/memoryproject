﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace MemoryProject.Utility {
    /// <summary>
    /// De BackgroundManager zorgt dat de achtergrondafbeelding van de Window correct werkt.
    /// </summary>
    public class BackgroundManager {
        public MainWindow window;
        public MediaElement bg;

        public BackgroundManager(MainWindow window) {
            this.window = window;

            bg = new MediaElement();
            bg.Stretch = Stretch.UniformToFill;
            bg.Visibility = Visibility.Hidden;
            bg.LoadedBehavior = MediaState.Manual;
            bg.UnloadedBehavior = MediaState.Manual;
            bg.MediaEnded += delegate (object source, RoutedEventArgs args) {
                bg.Position = TimeSpan.FromMilliseconds(1);
                bg.Play();
            };

            Grid.SetZIndex(bg, -100);
            Grid.SetRow(bg, 0);
            Grid.SetColumn(bg, 0);
            Grid.SetRowSpan(bg, 3);
            Grid.SetColumnSpan(bg, 3);


            void UpdateVisual(string theme) {
                bg.Source = new Uri(theme + "/bg.gif", UriKind.Relative);
                bg.Position = TimeSpan.FromMilliseconds(1);
                bg.Play();

                VisualBrush brush = new VisualBrush();
                brush.Visual = bg;
                window.Background = brush;
            }


            UpdateVisual((string) Settings.SettingsManager.Instance["Thema"].Value);

            Settings.SettingsManager.Instance["Thema"].AddListener(delegate (object oldValue, object newValue) {
                UpdateVisual((string) newValue);
            });
        }

        public void MakeVisible() {
            bg.Visibility = Visibility.Visible;
        }
    }
}
