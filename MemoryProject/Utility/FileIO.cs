﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;

namespace MemoryProject.Utility {
    /// <summary>
    /// Class die enkele functies bevat om interactie met bestanden te vermakkelijken.
    /// </summary>
    public static class FileIO {
        /// <summary>
        /// Lees het gegeven bestand als tekst.
        /// </summary>
        /// <param name="path">Het bestand dat gelezen moet worden.</param>
        /// <returns>Een string[] waarin ieder element een regel in het bestand is.</returns>
        public static string[] ReadText(string path) {
            try {
                string[] text = File.ReadAllLines(path);
                return text;
            } catch (Exception e) {
                MessageBox.Show("Er is een fout opgetreden tijdens het lezen van " + path + ": " + e.Message);
                throw e;
            }
        }


        /// <summary>
        /// Schrijf de tekst naar het gegeven bestand.
        /// </summary>
        /// <param name="path">Het bestand waarnaar geschreven moet worden.</param>
        /// <param name="text">De tekst die naar het bestand geschreven moet worden.</param>
        public static void WriteText(string path, string[] text) {
            try {
                if (!Directory.Exists(Path.GetDirectoryName(path))) Directory.CreateDirectory(Path.GetDirectoryName(path));

                File.WriteAllLines(path, text);
            } catch (Exception e) {
                MessageBox.Show("Er is een fout opgetreden tijdens het schrijven naar " + path + ": " + e.Message);
                throw e;
            }
        }


        /// <summary>
        /// Lees het gegeven bestand als binaire data.
        /// </summary>
        /// <param name="path">Het bestand dat gelezen moet worden.</param>
        /// <returns>Een byte[] waarin ieder element een byte in het bestand is.</returns>
        public static byte[] ReadData(string path) {
            try {
                byte[] data = File.ReadAllBytes(path);
                return data;
            } catch (Exception e) {
                MessageBox.Show("Er is een fout opgetreden tijdens het lezen van " + path + ": " + e.Message);
                throw e;
            }
        }


        /// <summary>
        /// Schrijf de binaire data naar het gegeven bestand.
        /// </summary>
        /// <param name="path">Het bestand waarnaar geschreven moet worden.</param>
        /// <param name="text">De binaire data die naar het bestand geschreven moet worden.</param>
        public static void WriteData(string path, byte[] data) {
            try {
                if (!Directory.Exists(Path.GetDirectoryName(path))) Directory.CreateDirectory(Path.GetDirectoryName(path));

                File.WriteAllBytes(path, data);
            } catch (Exception e) {
                MessageBox.Show("Er is een fout opgetreden tijdens het schrijven naar " + path + ": " + e.Message);
                throw e;
            }
        }


        /// <summary>
        /// Kijk of een bestand bestaat.
        /// </summary>
        /// <param name="path">Het bestand wiens bestaan geverifeerd moet worden.</param>
        /// <returns>Of het gegeven bestand bestaat of niet.</returns>
        public static bool Exists(string path) {
            return File.Exists(path);
        }


        /// <summary>
        /// Maak een lijst van alle mappen in een map.
        /// </summary>
        /// <param name="path">De map waarin zal worden gekeken.</param>
        /// <returns>Een lijst van mappen die zich in de gegeven map bevinden.</returns>
        public static string[] ListFolders(string path) {
            return Directory.GetDirectories(path);
        }


        /// <summary>
        /// Maak een lijst van alle bestanden in een map.
        /// </summary>
        /// <param name="path">De map waarin zal worden gekeken.</param>
        /// <returns>Een lijst van bestanden die zich in de gegeven map bevinden.</returns>
        public static string[] ListFiles(string path) {
            try {
                return Directory.GetFiles(path);
            } catch (IOException) {
                return new string[0];
            }
        }


        /// <summary>
        /// Lees een afbeelding vanuit een bestand.
        /// </summary>
        /// <param name="path">De afbeelding die geladen moet worden.</param>
        /// <returns>Een BitmapImage van de gegeven afbeelding.</returns>
        public static BitmapImage ReadImage(string path) {
            using (MemoryStream ms = new MemoryStream(ReadData(path))) {
                BitmapImage img = new BitmapImage();

                img.BeginInit();
                img.CacheOption = BitmapCacheOption.OnLoad;
                img.StreamSource = ms;
                img.EndInit();

                return img;
            }
        }
    }
}
