﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MemoryProject.Utility {
    public class GridPos {
        public int row, col;

        public GridPos(int row, int col) {
            this.row = row;
            this.col = col;
        }
    };
}
