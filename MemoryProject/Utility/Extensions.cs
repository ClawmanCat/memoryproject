﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MemoryProject.Utility {
    public static class Extensions {
        private static Random random = new Random();

        /// <summary>
        /// Gebruikt de Fisher-Yates shuffle om de volgorde van de items in een lijst te randomiseren.
        /// </summary>
        /// <typeparam name="T">Het type van de elementen van de lijst.</typeparam>
        /// <param name="list">De lijst die geshuffled moet worden.</param>
        public static void Shuffle<T>(this List<T> list) {
            int n = list.Count;

            while (n > 1) {
                n--;

                int k = random.Next(n + 1);
                list.Swap(n, k);
            }
        }

        /// <summary>
        /// Verwissel twee items in een lijst.
        /// </summary>
        /// <typeparam name="T">Het type van de elementen van de lijst.</typeparam>
        /// <param name="list">De lijst die de elementen bevat.</param>
        /// <param name="a">De index van het eerste element.</param>
        /// <param name="b">De index van het tweede element.</param>
        public static void Swap<T>(this List<T> list, int a, int b) {
            T temp = list[a];
            list[a] = list[b];
            list[b] = temp;
        }
    }
}
