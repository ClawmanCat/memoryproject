﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace MemoryProject.Utility {
    public static class SoundManager {
        private class Sound {
            public MediaPlayer mp;
            public double volume { get;
                set; }
            public bool isLooping;

            public Sound(MediaPlayer mp, double volume, bool isLooping) {
                this.mp = mp;
                this.volume = volume;
                this.isLooping = isLooping;
            }

            public void UpdateVolume(double globalVolume) {
                mp.Volume = this.volume * globalVolume;
            }

            public void SetLooping(bool loop) {
                void LoopingFunc(object source, EventArgs args) { Restart(); }

                if (loop && !isLooping) mp.MediaEnded += LoopingFunc;
                else if (!loop && isLooping) mp.MediaEnded -= LoopingFunc;
            }

            public void Restart() {
                mp.Position = TimeSpan.FromMilliseconds(1);
                mp.Play();
            }

            public void Stop() {
                mp.Stop();
            }
        }


        private static Dictionary<string, Sound> sounds = new Dictionary<string, Sound>();
        private static bool initialized = false;


        private static void InitListeners() {
            Settings.SettingsManager.Instance["Volume"].AddListener(delegate (object OldValue, object NewValue) {
                SetGlobalVolume(Double.Parse((string) NewValue));
            });

            Settings.SettingsManager.Instance["Effectvolume"].AddListener(delegate (object OldValue, object NewValue) {
                foreach (var pair in sounds) {
                    if (pair.Key != "theme") SetSoundVolume(pair.Key, Double.Parse((string)NewValue));
                }
            });

            Settings.SettingsManager.Instance["Muziekvolume"].AddListener(delegate (object OldValue, object NewValue) {
                SetSoundVolume("theme", Double.Parse((string)NewValue));
            });
        }
        
        public static bool PreLoadSound(string id, string theme = null) {
            if (!FileIO.Exists("./" + (theme == null ? (string) Settings.SettingsManager.Instance["Thema"].Value : theme) + "/sfx/" + id + ".wav")) return false;

            if (!initialized) {
                InitListeners();
                initialized = true;
            }

            MediaPlayer mp = new MediaPlayer();
            mp.Open(new Uri((theme == null ? (string) Settings.SettingsManager.Instance["Thema"].Value : theme) + "/sfx/" + id + ".wav", UriKind.Relative));

            double vol = (id == "theme") ?
                Double.Parse((string) Settings.SettingsManager.Instance["Muziekvolume"].Value) :
                Double.Parse((string) Settings.SettingsManager.Instance["Effectvolume"].Value);

            Sound s = new Sound(mp, vol, false);
            s.UpdateVolume(Double.Parse((string) Settings.SettingsManager.Instance["Volume"].Value));
            s.mp.Pause();
            s.mp.Position = TimeSpan.FromMilliseconds(1);

            sounds[id] = s;

            return true;
        }

        public static bool IsPlaying(string id) {
            if (!sounds.ContainsKey(id)) return false;

            // TODO: Vindt een betere manier om dit te doen.
            Sound s = sounds[id];
            TimeSpan a = s.mp.Position;
            System.Threading.Thread.Sleep(1);
            TimeSpan b = s.mp.Position;

            return a != b;
        }

        public static void ClearCache() {
            foreach (var sound in sounds) {
                sound.Value.Stop();
                sound.Value.mp.Close();
            }

            sounds.Clear();
        }

        public static bool AllLoaded() {
            foreach (var pair in sounds) {
                if (!pair.Value.mp.HasAudio) return false;
            }

            return true;
        }


        public static void PlaySound(string id, bool loop = false) {
            if (sounds.ContainsKey(id)) {
                Sound sound = sounds[id];
                sound.SetLooping(loop);
                sound.Restart();
            } else {
                if (PreLoadSound(id)) {
                    Sound sound = sounds[id];
                    sound.SetLooping(loop);
                    sound.mp.Play();
                }
            }
        }


        public static void StopSound(string id) {
            if (!sounds.ContainsKey(id)) return;
            sounds[id].Stop();
        }


        public static void SetSoundVolume(string id, double volume) {
            if (!sounds.ContainsKey(id)) return;

            sounds[id].volume = volume;
            sounds[id].UpdateVolume(Double.Parse((string) Settings.SettingsManager.Instance["Volume"].Value));
        }


        public static void SetGlobalVolume(double volume) {
            foreach (var pair in sounds) {
                pair.Value.UpdateVolume(volume);
            }
        }
    }
}
