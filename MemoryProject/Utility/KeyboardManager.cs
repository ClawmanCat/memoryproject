﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MemoryProject.Utility {
    /// <summary>
    /// Het indrukken van toetsen wordt alleen geregistreerd door de Window, en niet door de Page die zich in de Window bevindt.
    /// De Window stuurt daarom alle toetsenbordinput hierheen, zodat andere Pages er makkelijk gebruik van kunnen maken.
    /// </summary>
    public class KeyboardManager {
        public delegate void KeyboardListener(Key key, bool down);

        public static KeyboardManager Instance = new KeyboardManager();

        private List<KeyboardListener> listeners;

        private KeyboardManager() {
            this.listeners = new List<KeyboardListener>();
        }

        public void AddListener(KeyboardListener listener) { listeners.Add(listener); }
        public void RemoveListener(KeyboardListener listener) { listeners.Remove(listener); }

        public void DispatchKeypress(object source, KeyEventArgs args) {
            foreach (KeyboardListener listener in listeners) listener(args.Key, !args.IsUp);
        }
    }
}
